# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 20:31:07 2016  completed up to unit test pass Sat Dec 24 2016.
TODO: work out the logon issues (is not the easy answer of headers).
based on code in Week 2 L04-07
@author: Luke
"""

# check codebench exercise attempts
import urllib.request, urllib.parse, csv, http
from html.parser import HTMLParser


# unit testing switch
Attemptsite = 1   
# FOR ACTUAL WEBSITE USE MAKE THIS = 1

## Selections for unit test switch state:
#  attempt to logon to actual website (tried with valid username and password
if Attemptsite == 1:
    authentication_url = 'https://codebench.cecs.anu.edu.au/comp7230-s2-2016/login/?next=/comp7230-s2-2016/exercises/'
    payload = {
        'username': 'username',
        'password': 'password'
    }
    caf="C:/Users/Luke/Anaconda3/lib/site-packages/requests/cacert.pem"
    trythis = [1] #range(200)
    headers={'User-Agent': 'Mozilla/5.0'}

# unit test for locally saved file ( to be sure the class procesing works if page is located)
if Attemptsite == 0: 
    baseaddr="file:///C:/Users/Public/_Lpublic/_STUDY/_2016_S2_2_Compu/"
    caf="C:/Users/Public/_Lpublic/_STUDY/_2016_S2_2_Compu/codebench.cecs.anu.edu.au.crt"
    trythis = ("Week_2_ex01_py - CodeBench.htm","Week_6_ex17_py - CodeBench.htm")
    logindata=''
# begin class code to process contents
class CBHTPars(HTMLParser):
    """Parser to extract exercise attempts from codebench
       looking to populate:
         attemptlist: with [exerciseReference,TimeDateStampAttempted,Success(1),Fail(0)]
    """
    # members
    insidesubtag = False
    insideTry = False
    insideDo = False
    insidesubs = False
    lastDo = 0
    lastTry = 0
    lastExercise = ""
    nextTry = 0
    exclude = '\\r \\n'
    def handle_starttag(self,tag,attrs):
        # navbar list holds the exercise name 
        if (tag=="a"): 
            self.insidesubs = True
        # list of attempts are either success or failure
        if ((tag=="span") and (len(attrs)>0) and (attrs[0][1]=="badge badge-success")):
            self.insideDo=True
        if ((tag=="span") and (len(attrs)>0) and (attrs[0][1]=="badge badge-failure")):
            self.insideTry=True
        # each attempt also contains a time/date stamp for the attempt
        if (tag=="a" and (len(attrs)>0)):
            if (len(attrs)==2):
                if (attrs[1][1].strip(self.exclude)[:15]=="list-group-item"):
                    self.insidesubtag = True
    def handle_data(self,dat):
        if self.insidesubs == True and dat.strip(self.exclude)[:4] =='Week':
            self.lastExercise=dat.strip(self.exclude) 
        if self.insideDo == True:
            self.lastDo=1
        if self.insideTry == True:
            self.lastTry=1
        if (self.insidesubtag == True and dat.strip(self.exclude) not in ['','Correct!','Incorrect']):
            lastAttempt.append(dat.strip(self.exclude))
        # populate the list used for later CSV details, all bar the time/date stamp
        if (self.lastTry == 1 or self.lastDo == 1): 
            attemptlist.append(['','',0,0])
            attemptlist[self.nextTry] = [self.lastExercise,'',self.lastDo,self.lastTry]
            self.nextTry += 1
    def handle_endtag(self,tag):
        if(tag=="a"):
            self.insidesubs=False
        # this is where the time date stamp is populated
        if(tag=="a" and self.insidesubtag == True and lastAttempt[(self.nextTry-1)] not in ['','Correct!','Incorrect']):
            self.insidesubtag=False
            attemptlist[(self.nextTry-1)][1] = lastAttempt[(self.nextTry-1)]
        # reset the counters for success & failure.
        if(tag=="span") and self.insideDo:
            self.insideDo=False
            self.lastDo = 0
        if(tag=="span") and self.insideTry:
            self.insideTry=False 
            self.lastTry = 0
        

# begin the code outside the class 
fh=open("codebench_attempts_user.csv","w")
for exercise in trythis:
    attemptlist = [] 
    lastAttempt = []
    # Query the website
    data = urllib.parse.urlencode(payload)
    bdat = data.encode('UTF-8')
    if Attemptsite == 0:
        URL = baseaddr + str(exercise)
    if Attemptsite == 1:
        URL = authentication_url + str(exercise) + '/'
    req = urllib.request.Request(URL, data=bdat, headers = headers)
    resp = urllib.request.urlopen(req, cafile=caf)
    htIS = str(resp.read())
    resp.close()
    # use class to select correct tags
    parser=CBHTPars()
    #print(htIS)
    parser.feed(htIS)
    # Populate a csv with results
    for i in range(len(attemptlist)):
        if sum(attemptlist[i][2:])>0:
            csv.writer(fh).writerow(attemptlist[i])
fh.close()
