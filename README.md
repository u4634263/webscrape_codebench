Was built out of attempt to scrape codebench exercise attempts - does not work.

Able to test on locally saved .htm files correctly.  
(using Python 3 with annaconda - only tested from spyder ide)
requires: urllib, csv, http, html

Was not refactored for easy use might need hard keyed changes for:
 - username, password (assuming this part is possible).
 - local directories/filenames.
 - local copies of the three SSL cert keys required.
 - URL for class/period for your own attempts.
 
 TODO: resolve a HTTPError: FORBIDDEN when attempting a logon.



references (mostly because I was learning about ssl and login credentials):
comp7230 lecture 6? example for scraping bom.gov.au
http://stackoverflow.com/questions/27835619/ssl-certificate-verify-failed-error
http://stackoverflow.com/questions/27804710/python-urllib2-ssl-error/27826829#27826829
https://www.python.org/dev/peps/pep-0476/
http://stackoverflow.com/questions/11892729/how-to-log-in-to-a-website-using-pythons-requests-module
https://pythonprogramming.net/urllib-tutorial-python-3/